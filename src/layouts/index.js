import Vue from 'vue'
import store from './store/index'
import '../assets/styles/main.css'
import '../assets/iconfont/iconfont.css'

// 注册layouts下所有组件
const components = require.context('./', true, /\.vue$/)
components.keys().forEach(fileName => {
  const component = components(fileName)
  const componentName = component.default.name || fileName.replace(/\.\/(.*)\.vue/, '$1')
  Vue.component(componentName, component.default)
})

function install(Vue, options) {
  store.start(options)
  Vue.prototype.$layoutStore = store
}

store.install = install

export { default as Layout } from './Layout.vue'

export default store
