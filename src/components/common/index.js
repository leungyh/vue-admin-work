import Vue from 'vue'
// 读取组件
const components = require.context('./', true, /\.vue$/)
// 遍历组件路径
components.keys().forEach(fileName => {
  // 获取组件实例
  const component = components(fileName)
  // 获取组件名
  const componentName = component.default.name || fileName.replace(/\.\/(.*)\.vue/, '$1')
  // 注册组件
  Vue.component(componentName, component.default)
})
