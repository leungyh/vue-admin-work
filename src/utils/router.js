/* 路由权限与动态生成路由 */
import router from '@/router'
// 顶部进度条插件
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import store from '@/store'
import service from '@/api/axios.config'
import LayoutStore, { Layout } from '@/layouts/index'
import { routes as constantRoutes } from '@/router'
import { baseAddress, getMenuListByRoleId } from '@/api/url'

import Cookies from 'js-cookie'
import { toHump } from './utils'

// 进度环显示隐藏
NProgress.configure({ showSpinner: false })

// 获取菜单列表
function getRoutes() {
  return service({
    url: baseAddress + getMenuListByRoleId,
    method: 'POST',
    data: {
      userId: store.state.user.userId,
      roleId: store.state.user.roleId
    }
  }).then(res => {
    return generatorRoutes(res.data)
  })
}

// 引入组件
function getComponent(it) {
  return resolve => {
    if (it.children && it.children.length > 0) {
      require(['@/layouts/RouterViewLayout.vue'], resolve)
    } else {
      require(['@/views' + it.menuUrl], resolve)
    }
  }
}

// 获取含char数量
function getCharCount(str, char) {
  var regex = new RegExp(char, 'g')
  var result = str.match(regex)
  var count = !result ? 0 : result.length
  return count
}

// 是否主菜单
function isMenu(path) {
  return getCharCount(path, '/') === 1
}

// 获取路由名
function getNameByUrl(menuUrl) {
  const temp = menuUrl.split('/')
  return toHump(temp[temp.length - 1])
}

// 生成路由
function generatorRoutes(res) {
  const tempRoutes = []
  res.forEach(it => {
    const route = {
      path: it.menuUrl,
      name: getNameByUrl(it.menuUrl),
      // 当设置 true 的时候该路由不会在侧边栏出现，如login 404 等页面
      hidden: !!it.hidden,
      component: isMenu(it.menuUrl) ? Layout : getComponent(it),
      meta: {
        // 路由标签名字，主要用在 快捷标签 栏和导航栏中
        title: it.menuName,
        // 设置为true，标识着在 快捷标签 中不会有关闭按钮
        affix: !!it.affix,
        // 设置为true，标识着可以被<router-view/>组件缓存
        cacheable: !!it.cacheable,
        // 路由的图标信息
        icon: it.icon || '',
        // 路由的提示信息，目前有三种提示方式：new、小圆点、数字，对应的 tip：new、circle、12（具体的数字）
        tip: it.tip
      }
    }
    // 遍历
    if (it.children) {
      route.children = generatorRoutes(it.children)
    }
    tempRoutes.push(route)
  })
  return tempRoutes
}

// 判断是否登录
function isTokenExpired() {
  const token = Cookies.get('admin-token')
  return !!token
}

// 白名单路由
const whiteList = ['login']

// 全局前置守卫
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (whiteList.includes(to.name)) {
    next()
    NProgress.done()
  } else {
    if (!isTokenExpired()) {
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    } else {
      const isEmptyRoute = LayoutStore.isEmptyPermissionRoute()
      // 判断是否已经请求过路由
      if (isEmptyRoute) {
        // 加载路由
        const accessRoutes = []
        getRoutes().then(async routes => {
          accessRoutes.push(...routes)
          accessRoutes.push({
            path: '*',
            redirect: '/404',
            hidden: true
          })
          LayoutStore.initPermissionRoute([...constantRoutes, ...accessRoutes])
          router.addRoutes(accessRoutes)
          next({ ...to, replace: true })
        })
      } else {
        next()
      }
    }
  }
})

// 全局后置钩子
router.afterEach((to, from) => {
  NProgress.done()
})
