import permission from './permission'

permission.install = function (Vue) {
  // 创建全局自定义指令
  Vue.directive('permission', permission)
}

export default permission
