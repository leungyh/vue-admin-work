const path = require('path')
const CompressionPlugin = require('compression-webpack-plugin')
// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/vue-admin-work' : '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: true,
  productionSourceMap: false,
  devServer: {
    hot: true,
    port: 5566,
    open: true,
    inline: true
    // proxy: {}
  },
  chainWebpack(config) {
    if (process.env.NODE_ENV === 'production') {
      // gzip可以很大程度减少包的大小，提高加载速度
      config.plugin('compressionPlugin')
        .use(new CompressionPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.js$|\.html$|.\css/,
          threshold: 10240,
          deleteOriginalAssets: false
        }))

        // 使用打包分析工具
        // config.plugins.push(new BundleAnalyzerPlugin());
        // 使用“terser-webpack-plugin”去除console
        // config.optimization.minimizer[0].options.terserOptions = {
        //     compress: {
        //         warnings: false, // 是否移除未使用的声明等显示警告
        //         drop_console: true, // 注释console
        //         drop_debugger: true,
        //         pure_funcs: ["console.log"] // 移除console
        //     }
        // };
    }

    // 移除 prefetch 插件
    config.plugins.delete('prefetch-index')
    // 移除 preload 插件
    config.plugins.delete('preload-index')

    // 配置svg
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}
